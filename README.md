# Description
The requirement was to create a service that gives the latest and historical exchange rate for the currency. Three-tier network architecture had to be used to create this project. Full-stack development was chosen to approach this problem. For frontend HTML, CSS and React framework and for backend, NestJS framework was used. Further Typescript was utilised to aid the development of both frontend and backend. This project also includes implementation of Gitlab CI/CD for faster deployments.

# Basis of technical choices

## React
React has been chosen as the frontend Javascript framework as it provides an environment that is component driven thus enhancing the modularity of the overall code. It provides reusability of components, reducing development time and improving the performance of the application. Redux-toolkit has also been used with react. It significantly improves the state management capability of React and helps in the creation of container-based components that further assists in the separation of presentation logic, data processing logic and network logic. 

## NestJS 
NestJS has been chosen as the backend NodeJS framework as it is highly scalable and provides exceptional support for TypeScript. It provides a dependency injection system that efficiently initializes dependencies that are easily reusable. It aids the following of SOLID principles of software engineering. All these factors make NestJS highly testable.

## Architecture
The microservice architecture has been selected for the backend because of its high scalability and ability to make a robust application. Moreover, pub/sub pattern can be implemented effectively in this type of architecture.

# Trade-offs and Features that could be implemented better
- Implementation of CI/CD pipeline would have enabled faster deployment, early bug detection and more frequent feature/bugfix releases.
- Dockerization of microservices which could maintain consistency in the environment and package management. Also, provide easier deployment of microservices.
- If the design system at the frontend was developed beforehand it would increase the speed of component creation without extra effort on styling.
- Logger implementation.
- Data monitoring.
- Creation of notification service which would work with Pub / Sub message broker like Redis for real-time updates.
- Integration of database for user data persistence and use cases/ features.
- Better error handling.
- Better UI/UX experience.

## Links

- Hosted on: [HerokuApp with AWS EC2 backend](https://exrate-app.herokuapp.com/)
- Repo Group: [exchange-rate1](https://gitlab.com/exchange-rate1)
- Frontend: [exchange-frontend](https://gitlab.com/exchange-rate1/exchange-frontend)
- Backend: [exchange-rate](https://gitlab.com/exchange-rate1/exchange-rate) 
- Microservices: [exchange-latest](https://gitlab.com/exchange-rate1/exchange-latest) [exchange-historical](https://gitlab.com/exchange-rate1/exchange-historical)

## Author profile links
- Github: [prinz1208](https://github.com/prinzz1208)
- Resume: [Priyanshu Agarwal](https://drive.google.com/file/d/1683TqMqjPcTMPkZw1Y8aX3uC288lzcjO/view?usp=sharing)
