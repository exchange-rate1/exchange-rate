import { HttpException, Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { GeneralException } from './shared/exceptions/handlers';
import { MicroserviceException } from './shared/exceptions/handlers/microservice-exception.handler';
import { ApiErrors } from './shared/response/messages';
import { GetExRateResponse } from './types';

@Injectable()
export class AppService {
  constructor(
    @Inject(process.env.HISTORICAL_SERVICE_TOKEN)
    private readonly historicalService: ClientProxy,
    @Inject(process.env.LATEST_SERVICE_TOKEN)
    private readonly latestService: ClientProxy,
  ) {}

  // to get historical exchange rates according to date.
  async getHistoricalData(date: Date): Promise<GetExRateResponse> {
    try {
      // date format tranformation to YYYY-MM-DD.
      const dateString =
        date.getFullYear() +
        '-' +
        `${date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth()}` +
        '-' +
        `${date.getDay() < 10 ? '0' + date.getDay() : date.getDay()}`;

      // call to Exchange-Historical Microservice
      const res = await this.historicalService
        .send<GetExRateResponse>('get_historical_data', dateString)
        .toPromise();

      if (!res) {
        Logger.error(res);
        throw new GeneralException(ApiErrors.GeneralError);
      }
      return res;
    } catch (err) {
      Logger.error(err);
      if (err.response instanceof HttpException) {
        const { data, status } = err.response;
        throw new MicroserviceException(data, status);
      }
      throw new GeneralException(ApiErrors.SystemErrorMessage);
    }
  }

  // to get latest exchange rates.
  async getLatestData(): Promise<GetExRateResponse> {
    try {
      // call to Exchange-Latest Microservice
      const res = await this.latestService
        .send<GetExRateResponse>('get_latest_data', 'hello World')
        .toPromise();
      if (!res) {
        Logger.error(res);
        throw new GeneralException(ApiErrors.GeneralError);
      }
      return res;
    } catch (err) {
      Logger.error(err);
      if (err.response instanceof HttpException) {
        const { data, status } = err.response;
        throw new MicroserviceException(data, status);
      }
      throw new GeneralException(ApiErrors.SystemErrorMessage);
    }
  }

  // conversion of amount from one currency to other.
  async convert(amount: number, from: string, to: string): Promise<number> {
    try {
      const res = await this.latestService
        .send<number>('convert', { amount, from, to })
        .toPromise();

      // for conversion, response can be 0 which is falsy.
      // thus added undefined or null check.
      if (res === undefined || res === null) {
        throw new GeneralException(ApiErrors.GeneralError);
      }
      return res;
    } catch (err) {
      if (err.response instanceof HttpException) {
        const { data, status } = err.response;
        throw new MicroserviceException(data, status);
      }
    }
  }
}
