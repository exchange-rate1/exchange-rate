import { Type } from 'class-transformer';
import { IsDate, IsOptional } from 'class-validator';

export class GetExRateDto {
  @IsOptional()
  @Type(() => Date)
  @IsDate()
  date: Date;
}
