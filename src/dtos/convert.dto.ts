import { Type } from 'class-transformer';
import { IsInt, IsString, Length } from 'class-validator';

export class ConvertDto {
  @IsString()
  @Length(3, 3)
  from: string;

  @IsString()
  @Length(3, 3)
  to: string;

  @IsInt()
  @Type(() => Number)
  amount: number;
}
