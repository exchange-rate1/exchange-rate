import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppConfigModule } from './app-config/app-config.module';
import { AppConfigService } from './app-config/app-config.service';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    AppConfigModule,
    ClientsModule.registerAsync([
      {
        name: process.env.HISTORICAL_SERVICE_TOKEN,
        useFactory: async (appConfigService: AppConfigService) => ({
          transport: Transport.TCP,
          options: {
            host: appConfigService.microServices.host,
            port: appConfigService.microServices.historicalService.port,
          },
        }),
        inject: [AppConfigService],
      },
    ]),
    ClientsModule.registerAsync([
      {
        name: process.env.LATEST_SERVICE_TOKEN,
        useFactory: async (appConfigService: AppConfigService) => ({
          transport: Transport.TCP,
          options: {
            host: appConfigService.microServices.host,
            port: appConfigService.microServices.latestService.port,
          },
        }),
        inject: [AppConfigService],
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
