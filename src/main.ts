import { ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { AppConfigService } from './app-config/app-config.service';
import { AppModule } from './app.module';
import {
  GeneralExceptionFilter,
  ValidationExceptionFilter,
} from './shared/exceptions/filters';
import { MicroserviceExceptionFilter } from './shared/exceptions/filters/microservice-exception.filter';
import { ApiResponseInterceptor } from './shared/interceptor/api-response.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  app.useGlobalInterceptors(new ApiResponseInterceptor(app.get(Reflector)));
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalFilters(
    new GeneralExceptionFilter(),
    new ValidationExceptionFilter(),
    new MicroserviceExceptionFilter(),
  );
  const appConfigService = app.get(AppConfigService);
  await app.listen(appConfigService.port);
}
bootstrap();
