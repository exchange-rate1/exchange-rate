export default () => ({
  port: parseInt(process.env.PORT) || 3000,
  microServices: {
    host: process.env.HOST,
    historicalService: {
      token: process.env.HISTORICAL_SERVICE_TOKEN,
      port: parseInt(process.env.HISTORICAL_SERVICE_PORT),
    },
    latestService: {
      token: process.env.LATEST_SERVICE_TOKEN,
      port: parseInt(process.env.LATEST_SERVICE_PORT),
    },
  },
});
