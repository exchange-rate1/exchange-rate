import * as Joi from 'joi';

const schema = Joi.object({
  PORT: Joi.number().required(),
  MICROSERVICES_HOST: Joi.string().required(),
  HISTORICAL_SERVICE_TOKEN: Joi.string().required(),
  HISTORICAL_SERVICE_PORT: Joi.number().required(),
});

export default schema;
