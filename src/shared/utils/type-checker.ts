export const isString = (obj: any): boolean => {
  return typeof obj === 'string' || obj instanceof String;
};
