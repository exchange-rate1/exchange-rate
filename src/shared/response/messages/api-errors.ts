import { HttpStatus } from '@nestjs/common';

export interface ApiError {
  message: string;
  code: number;
  httpStatus: HttpStatus;
}

const errorCreator = (
  message: string,
  code: number,
  httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
): ApiError => ({ message, code, httpStatus });

export const ApiErrors = {
  SystemErrorMessage: errorCreator(
    'Internal server error, please try again later',
    100,
  ),
  GeneralError: errorCreator(
    'Something went wrong please try again later',
    100,
  ),
};
