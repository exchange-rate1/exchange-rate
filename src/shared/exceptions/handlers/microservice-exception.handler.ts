import { HttpException, HttpStatus } from '@nestjs/common';
import { ApiResponseError } from '../../types/api-response.interface';

export class MicroserviceException extends HttpException {
  constructor(data: ApiResponseError, httpStatus: HttpStatus) {
    super(data, httpStatus);
  }
}
