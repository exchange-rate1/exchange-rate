import { HttpException } from '@nestjs/common';
import { ApiError } from '../../response/messages/api-errors';

export class GeneralException extends HttpException {
  constructor(error: ApiError, message?: string) {
    if (message) {
      error.message = message;
    }
    super(error, error.httpStatus);
  }
}
