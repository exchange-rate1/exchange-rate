import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import { ApiResponseError } from '../../types/api-response.interface';
import { MicroserviceException } from '../handlers/microservice-exception.handler';

@Catch(MicroserviceException)
export class MicroserviceExceptionFilter implements ExceptionFilter {
  catch(exception: MicroserviceException, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse<Response>();

    const status = exception.getStatus();
    const data = exception.getResponse() as ApiResponseError;

    response.status(status).json(data);
  }
}
