export enum ExceptionTypes {
  System = 'system',
  Validation = 'validation',
  General = 'general',
}
