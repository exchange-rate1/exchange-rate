import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { ConvertDto } from './dtos/convert.dto';
import { GetExRateDto } from './dtos/get-exrate.dto';
import { GetExRateResponse } from './types';

@Controller('/')
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get('/ping')
  ping() {
    return 'pong';
  }

  @Get('/exrates')
  async getExchangeRates(
    @Query() { date }: GetExRateDto,
  ): Promise<GetExRateResponse> {
    if (date) {
      return this.appService.getHistoricalData(date);
    }
    return this.appService.getLatestData();
  }

  @Get('/convert')
  async getConvertedRates(
    @Query() { amount, from, to }: ConvertDto,
  ): Promise<number> {
    return this.appService.convert(amount, from, to);
  }
}
